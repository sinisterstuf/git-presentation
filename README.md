Collaborative Development
=========================

*Using git and Eclipse with the EGit and Mylyn plugins.*

A presentation in LaTeX with Beamer, which I wrote to help present how git
can be used to work collaboratively from within the Eclipse IDE.

[![PDF Status](https://www.sharelatex.com/github/repos/sinisterstuf/git-presentation/builds/latest/badge.svg)](https://www.sharelatex.com/github/repos/sinisterstuf/git-presentation/builds/latest/output.pdf)
[![Creative Commons Attribution-ShareAlike](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)](http://creativecommons.org/licenses/by-sa/4.0/)

This work is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).
